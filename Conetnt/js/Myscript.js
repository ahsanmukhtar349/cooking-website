
$(window).scroll(function () {
    if ($(window).scrollTop() >= 30) {

        $('.nav').addClass('fixed-header');

    }

    else {

        $('.nav').removeClass('fixed-header');

    }
});


$(window).scroll(function () {
    if ($(window).scrollTop() >= 1 && $(window).width() >= 992) {     

         $('.triangle-down').addClass('fixed-logo');

         $('.logo').addClass('fix-logo');


    }


    else {
        $('.triangle-down').removeClass('fixed-logo');

        $('.logo').removeClass('fix-logo');

    }

    if ($(window).scrollTop() >= 30 && $(window).width() >= 320 && $(window).width() <= 991 ) {     

        $('.triangle-down').addClass('fixed-logo1');

        $('.logo').addClass('fix-logo1');


   }


   else {
       $('.triangle-down').removeClass('fixed-logo1');

       $('.logo').removeClass('fix-logo1');

   }

});
var Owl = {

    init: function () {
        Owl.carousel();
    },

    carousel: function () {
        var owl;
        $(document).ready(function () {

            owl = $('.choosecarousel').owlCarousel({

                // stagePadding: 150,
                nav: false,
                dots: true,
                loop: true,
                autoplay: true,
                margin: 0, 
                dotsEach: 1,
                dotsContainer: '.test',
                responsive: {
                    0: {
                        items: 1,
                       
                    },
                    500: {
                        items: 2,

                    },
                    800: {
                        items: 3,

                    },
                    992: {
                        item: 3
                    },
                    1200: {
                        items: 3,
                       
                    },

                }

            });


            $('.Customdots').on('click', 'li', function (e) {
                owl.trigger('to.owl.carousel', [$(this).index(), 500]);
            });
        });
    }
};
$(document).ready(function () {
    Owl.init();
});



$('.aboutcarousel').owlCarousel({
    loop: true,
    // margin: 30, 
    dots: false,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    nav: true,
    navText: ["<div class='btn1'> <i class='glyphicon glyphicon-arrow-left navleft' ></i></div>",
        "<div class='btn2'><i class='glyphicon glyphicon-arrow-right navright'></i></div>"],
    responsive: {
        0: {
            items: 1
        },
        735: {
            items: 2
        },
        1000: {
            items: 2
        }
    }

})


$('.dishecarousel').owlCarousel({
    loop: true,
    // margin: 30, 
    dots: false,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    nav: false,
    responsive: {
        0: {
            items: 1
        },
        350: {
            items: 2
        },
        500: {
            items: 3
        },
        768: {
            items: 4
        },
        992: {
            items: 5
        },
        1200: {
            items: 6
        },

    }

})


$('.dishescarousel').owlCarousel({
    loop: true,
    dots: true,
    dotsEach: 3,
    autoplay: true,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    nav: false,
    responsive: {
        0: {
            items: 1

        },
        350: {
            items: 2
        },
        500: {
            items: 3
        },
        768: {
            items: 4
        },
        992: {
            items: 5
        },
        1200: {
            items: 6
        },

    }

})


